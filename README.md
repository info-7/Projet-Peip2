Ce code est une simulation qui permet de tester les algorithmes développés dans le cadre du Projet INFO 7 de Polytech Lyon en deuxième année.

Pour compiler le code, vous aurez besoin d'un système linux avec make.

Il vous suffira de lancer la commande Make dans le dossier racine pour lancer la compilation puis de lancer ./bin/exec pour lancer le programme.
Les paramètres peuvent être ajustés dans global .h et nécessite d'utiliser make clean suivi de make pour recompiler le programme.
