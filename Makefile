#SRCS_SFML = Car.cpp ChargingSpot.cpp Communicator.cpp CustomMainWindow.cpp CustomSecondWindow.cpp Ilot.cpp Infrastructure.cpp Infrastructure_Manager.cpp main.cpp Pnt-interet.cpp Road.cpp RoadsNetwork.cpp Station.cpp
SRCS_SFML = $(shell ls src/ | grep ".cpp")

FINAL_TARGET_SFML = exec


ifeq ($(OS),Windows_NT)
	INCLUDE_DIR_SFML = 	-Iextern/SFML-2.5.1/include

	LIBS_SFML = -Lextern \
			-Lextern/SFML-2.5.1/lib \
			-lmingw32 -lsfml-graphics -lsfml-window -lsfml-system \

else
	INCLUDE_DIR_SFML = -Iextern/SFML-2.5.1/include
	LIBS_SFML =	-Lextern/SFML-2.5.1/lib\
				-lGL -lGLU\
				-lsfml-graphics -lsfml-window -lsfml-system
endif

CC					= g++
LD 					= g++
LDFLAGS  			= -Wl,--rpath=./extern/SFML-2.5.1/lib
CPPFLAGS 			= -Wall -ggdb -fPIC  #-O2   # pour optimiser
OBJ_DIR 			= obj
SRC_DIR 			= src
BIN_DIR 			= bin
INCLUDE_DIR			= -Isrc -Iinclude -Iextern/imgui

#LIB_FLAGS = -L

default: make_dir $(BIN_DIR)/$(FINAL_TARGET_SFML)

make_dir:
ifeq ($(OS),Windows_NT)
	if not exist $(OBJ_DIR) mkdir $(OBJ_DIR) $(OBJ_DIR)\sfml $(OBJ_DIR)\core
else
	test -d $(OBJ_DIR) || mkdir -p $(OBJ_DIR) $(OBJ_DIR)/sfml $(OBJ_DIR)/core
	
endif

$(BIN_DIR)/$(FINAL_TARGET_SFML): $(SRCS_SFML:%.cpp=$(OBJ_DIR)/%.o)
	$(LD)  $(CPPFLAGS) $(LDFLAGS) $+ -o $@ $(LDFLAGS) $(LIBS_SFML)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC)  $(CPPFLAGS) -c $(CPPFLAGS) $(INCLUDE_DIR_SFML) $(INCLUDE_DIR) $< -o $@

clean:
ifeq ($(OS),Windows_NT)
	del /f $(OBJ_DIR)\txt\*.o $(OBJ_DIR)\sfml\*.o $(OBJ_DIR)\core\*.o $(BIN_DIR)\$(FINAL_TARGET_SFML).exe
else
	rm -rf $(OBJ_DIR) $(BIN_DIR)/$(FINAL_TARGET_SFML)
endif

