#include "RoadsNetwork.h"

RoadsNetwork::RoadsNetwork(RenderWindow *p_window)

{
	window = p_window;
	
	//Construction de la grille de routes
	{
		//Construction des routes horizontales
		for (int x = 0; x < nbRoadX; x++)
		{

			hRoadsPoints[x][0] = marginRS;
			hRoadsPoints[x][1] = marginRS + x * dy;
			hRoadsPoints[x][2] = hRoadsPoints[x][0] + screenX - 2 * marginRS;
			hRoadsPoints[x][3] = hRoadsPoints[x][1] + roadSize;

			//Calcul des positionnement verticaux des ilots
			posYIndex[x] = hRoadsPoints[x][1] + 0.6 * dy;
		}

		//Construction des routes verticales
		for (int x = 0; x < nbRoadY; x++)
		{
			vRoadsPoints[x][0] = marginRS + x * dx;
			vRoadsPoints[x][1] = marginRS;
			vRoadsPoints[x][2] = vRoadsPoints[x][0] + roadSize;
			vRoadsPoints[x][3] = vRoadsPoints[x][1] + screenY - 2 * marginRS;

			//Calcul des positionnement horizontale des ilots
			posXIndex[x] = vRoadsPoints[x][0] + 0.33 * dx;
		}
	}
}

void RoadsNetwork::buildRoads()
{
	Road r;
	//Construction de la grille de routes
	{
		//Construction des routes horizontales
		for (int x = 0; x < nbRoadX; x++)
		{
			r.setCaracts(hRoadsPoints[x][0], hRoadsPoints[x][1], Vector2f(hRoadsPoints[x][2] - hRoadsPoints[x][0] - 2, roadSize));
			window->draw(r);
		}

		//Construction des routes verticales
		for (int x = 0; x < nbRoadY; x++)
		{
			r.setCaracts(marginRS + x * dx, marginRS, Vector2f(roadSize, vRoadsPoints[x][3] - vRoadsPoints[x][1] - 2));
			window->draw(r);
		}
	}
}
