#ifndef STATION_CPP
#define STATION_CPP

#include "ChargingSpot.h"
#include "Ilot.h"
#include "RoadsNetwork.h"
#include "global.h"
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <string.h>
#include <time.h>
using namespace sf;
using namespace std;

/**
 * @brief classe définissant la station de recharge à prix fixe
 */
class Station : public ChargingSpot
{
private:
public:
	int posI = 1;

 /**
  * @brief Permet de construire la station et de lui donner une position sur la carte
  * @param p_rn
  */
	Station(RoadsNetwork *p_rn);

 /**
  * @brief Met à jour le texte à côté de la station sur la fenêtre
  * @return void
  */
	virtual void updateInfoString();

 /**
  * @brief renvoie le type de destination
  * @return string
  */
	virtual string getDestType();


 /**
  * @brief Place la station au centre de la carte
  * @return (void)
  */
	void calculposI();
};
#endif