#ifndef COMM_CPP
#define COMM_CPP

#include "global.h"
#include <iostream>
#include <queue>

using namespace std;

//le format de trame est le suivant : adresse du demandeur, tableau requete ci dessous, valeur en float

class Communicator;

/**
 * @brief structure définissant la trame des communications
 */
struct requete
	{
		Communicator* ad; ///< cette structure envoie son adresse mémoire, un tableau qui spécifie la requête et une valeur en float
		bool spec[5];	  ///tableau de booléens permettant de savoir la requête envoyée : Q R O N V (permet de spécifier le type de requete)
		float valeur = -1;
		int charge = -1;
		bool failed = false;
	};

/**
 * @brief structure définissant une carte réseau
 */
class Communicator
{
protected:
	/**
 * cette structure permet d'envoyer la trame d'un bloc dans la file queue
 */
	

	requete R;

public:
	float valeur;

	std::queue<requete>* buff; ///File contenant les requêtes à traiter

 /**
  * @brief Constructeur de la classe
  */
	Communicator();
 /**
  * @brief Destructeur de la classe
  */
	~Communicator(); 

 /**
  * @brief envoie la structure requete dans le "queue" de l'objet ciblé en paramètre par son adresse mémoire
  * @param recepteur
  * @return (void)
  */
	void send(const Communicator* recepteur);

 /**
  * @brief Fonction envoyant la trame nécessaire pour avoir comme réponse le prix de l'énergie à un endroit
  * @param I un communicator
  * @return (void)
  */
	void askPrice(const Communicator* I);


 /**
  * @brief envoie le prix actuelle au demandeur
  * @param V un communicator
  * @param int
  * @param int
  * @return 
  */
	virtual void sendPrice(const Communicator* V, unsigned int prix, unsigned int charge=-1);

 /**
  * @brief Envoie oui au comunicator en paramètres
  * @param V
  * @return (void)
  */
	void sendYes(const Communicator* V);

 /**
  * @brief Envoie non au comunicator en paramètres
  * @param V
  * @return (void)
  */
	void sendNo(const Communicator* V);

 /**
  * @brief décide si la transaction peut être faite avec le prix et le niveau de charge demandé en paramètres
  * @param Communicator
  * @param valeur
  * @param charge
  * @return void
  */
	virtual void Qvaleur(const Communicator*, float valeur, int charge = -1);
 /**
  * @brief Exécute l'action définie une fois oui reçu
  * @param Communicator
  * @return void
  */
	virtual void actionOui(const Communicator*) = 0;
 /**
  * @brief Exécute l'action définie une fois non reçu
  * @param Communicator
  * @return void
  */
	virtual void actionNon(const Communicator*) = 0;
 /**
  * @brief Exécute l'action définie une fois le prix d'un point de charge reçu (pour voiture on demande une modification du prix)
  * @param Communicator
  * @param price
  * @return void 
  */
	virtual void actionPrix(const Communicator*, float price = 0) = 0;
 /**
  * @brief EXécute l'action demandé si une transaction n'a pas pu être effectuée
  * @param Communicator
  * @return void
  */
	virtual void actionFailed(const Communicator*);

 /**
  * @brief fonction permettant de traiter les requêtes en attente dans la file
  * @param int
  * @return (void)
  */
	void processQueue(unsigned int tprix = 0);
};

#endif