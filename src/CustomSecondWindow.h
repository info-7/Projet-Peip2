#ifndef CUSTOM_SECOND_WINDOW_H
#define CUSTOM_SECOND_WINDOW_H
#include "Car.h"
#include "CustomMainWindow.h"
#include "global.h"
#include "Pnt-interet.h"
#include "Road.h"
#include "RoadsNetwork.h"
#include "Station.h"
#include <SFML/Graphics.hpp>
#include <ctime>
#include <math.h>
#include <stdlib.h>

using namespace std;
using namespace sf;

class CustomSW : public RenderWindow
{
public:
    Car **voiture;
    time_t timeFromBeg = time(NULL);

    CustomSW(VideoMode, String);
    void init();
    ~CustomSW();
    void handleWindow();
};

#endif