#include "Communicator.h"

Communicator::Communicator()
{
	buff = new queue<requete>;
	R.ad = this;
}


Communicator::~Communicator()
{
	delete buff;
}

/**
 * envoie la structure requete dans le "queue" de l'objet ciblé en paramètre par son adresse mémoire
 */
void Communicator::send(const Communicator *recepteur)
{
	recepteur->buff->push(R);
}

void Communicator::askPrice(const Communicator *I) ///fonction d'envoi/reception pour les voitures
{
	// cout<<"askPrice 2"<<endl;
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[0] = true; //Q=true
	send(I);
}

/*void Communicator::askModif(const Communicator *I, float newPrice) ///fonction d'envoi/reception pour les voitures
{
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[0] = true; //Q=true
	R.spec[4] = true; //V=true
	valeur = newPrice;
	send(I);
}
*/
void Communicator::sendPrice(const Communicator *V, unsigned int prix, unsigned int charge) ///fonctions d'envoi réception de l'ilot
{
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[1] = true; //R=true
	R.spec[4] = true; //V=true
	R.valeur = prix;
	send(V);
}

void Communicator::sendYes(const Communicator *V) ///fonctions d'envoi réception de l'ilot
{
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[1] = true; //R=true
	R.spec[2] = true; //O=true
	send(V);
	// cout<<"sendYes"<<endl;
}

void Communicator::sendNo(const Communicator *V) ///fonctions d'envoi réception de l'ilot
{
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[1] = true; //R=true
	R.spec[3] = true; //N=true
	send(V);
	// cout<<"sendNo"<<endl;
}

void Communicator::Qvaleur(const Communicator*, float valeur, int charge){}


void Communicator::processQueue(unsigned int tprix)
{
	while (!buff->empty())
	{	
		requete temp;
		temp.ad = buff->front().ad;
		for (unsigned int i = 0; i < 5; i++)
		{
			temp.spec[i] = buff->front().spec[i];
		}
		temp.valeur = buff->front().valeur;
		temp.charge = buff->front().charge;
		temp.failed = buff->front().failed;
		buff->pop();
		if (temp.failed){
				actionFailed(temp.ad);
			}
		else if (temp.spec[0]) //Q=true
		{
			if (temp.spec[4]) //V=true
			{
				// cout<<"Qvaleur"<<endl;
				Qvaleur(temp.ad, temp.valeur, temp.charge);
			}
			else //V=false
			{
				// cout<<"askPrice répondu"<<endl;
				sendPrice(temp.ad, tprix, temp.charge);
			}
		}
		else
		{
			if (temp.spec[1])
			{
				if (temp.spec[2])
				{
					// cout<<"action Oui"<<endl;
					actionOui(temp.ad);
				}
				else
				{
					if (temp.spec[3])
					{
						// cout<<"actionNon"<<endl;
						actionNon(temp.ad);
					}
					else
					{						
						actionPrix(temp.ad, temp.valeur);
					}
				}
			}
		}
	}
}

void Communicator::actionFailed(const Communicator*){}