#ifndef ILOT_H
#define ILOT_H

#include "ChargingSpot.h"
#include "global.h"
#include "RoadsNetwork.h"
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace sf;

class Ilot : public ChargingSpot
{

public:
	
	Ilot(RoadsNetwork *p_rn);

	/**
	 * @brief Met à jour la string informative
	 * 
	 */
	virtual void updateInfoString();

	/**
	 * @brief Retourne le type de l'infrastructure
	 * 
	 * @return string 
	 */
	virtual string getDestType();

	/**
	 * @brief Met à jour du prix en fonction de la charge
	 * 
	 */
	void updatePrice();
};

#endif