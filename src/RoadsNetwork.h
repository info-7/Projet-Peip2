#ifndef ROADS_NETWORK_H
#define ROADS_NETWORK_H

#include "Road.h"
#include "global.h"
class RoadsNetwork
{
public:
	RenderWindow* window; /**< Pointeur vers la fenêtre de rendu principales */

	int roadSize = 200 / nbRoadX; /**< Largeur de route */
	float dy = (((float)screenY) - 2.f * ((float)marginRS) - ((float)roadSize)) / (((float)nbRoadX - 1)); /**< dy -> espace entre les routes horizontales */

	float dx = (((float)screenX) - 2.f * ((float)marginRS) - ((float)roadSize)) / (((float)nbRoadY - 1)); /**< dx -> écart selon X des routes verticales */

	float customScale = ((float)roadSize) / ((float)100) * 0.6; /**< Constante de mise à l'échelle des éléments */

	/**
*	Dans chaque tableau sont stockées les coordonnées des deux points caractérisant la route en premier indice du tableau (qui est un rectangle).
*	de cette manière : x1, y1, x2, y2.
*	où x1,y1 sont les coordonnées du point supérieur gauche et x2,y2, les coordonnées du point inférieur droit.
*/
	int hRoadsPoints[nbRoadX][4], vRoadsPoints[nbRoadY][4];

	int posXIndex[nbRoadY], posYIndex[nbRoadX]; /**< Points contenants les positions des centres des cellules */

	RoadsNetwork(RenderWindow* p_window);

	/**
	 * @brief Construits les routes
	 * 
	 */
	void buildRoads();
};

#endif