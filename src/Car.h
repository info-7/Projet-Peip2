#ifndef CAR_CPP
#define CAR_CPP

#include "Communicator.h"
#include "global.h"
#include "Ilot.h"
#include "Pnt-interet.h"
#include "RoadsNetwork.h"
#include "Infrastructure_Manager.h"
#include <SFML/Graphics.hpp>
#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <math.h>

using namespace std;

using namespace sf;

/**
 * @brief Structure de communication côté voiture
 * 
 */
struct IlotNegoc
{
    ChargingSpot *I;
    float prix_negoc = 0;
    bool ilotOK = false;
    unsigned int tries = 0;
};

class Car : public Sprite, public Communicator
{
private:
    /** Niveau de batterie en %1000 */
    unsigned int chargeLevel = 10000 + rand() % 90000;
    time_t timeParked;                 /// Temps garé
    const float vmax = simuSpeed * 50; /**< Vitesse du vehicule en km/h*/
    const float waitingTime = 6.00;    /// Temps d'attente quand garé

public:
    bool parked = false;                      /// Est garé ?
    int wallet = sommeOrigine;                /// Porte monnaie voiture
    RoadsNetwork *rn;                         /// Pointeur vers le réseau auto-routier
    InfraManager *infraManager;               /// Pointeur vers le gestionnaire d'infrastructures
    IlotNegoc TabIlot[5];                     /// Tableau d'ilots selectionné pour la négociation
    bool waitAnswers = false;                 /// Attend des réponses des ilots ?
    float speedX = ((float)vmax), speedY = 0; /// Vitesse horizontale / verticale

    float coordX, coordY; /// Cordonnées dans le repère de la grille

    float constantSpeedProcess; /// Constante de vitesse calculée en fonction du rafraîchissement

    Infrastructure *dest = nullptr; /// Infrastructure de destination

    int RoadXused = -1, RoadYused = -1; /// Routes actuellement emprunté
    bool isOnACrossRoad = true;         /// Est sur une intersection ?

    Texture texture;                                               /// Texture de la voiture
    Image carImg;                                                  /// Image de la voiture
    bool colorized = false;                                        /// Est colorée ?
    int nbRequete = 0, nbDenieds = 0, nbFailed = 0, nbSuccess = 0; /// Informations de communication
    Font police;                                                   /// Police d'écriture
    Text text;

    Car(RoadsNetwork *rn, InfraManager *im);

    ~Car();
    /**
     * @brief Positionne le vehicule et son texte
     * 
     */

    void setPos(const float x, const float y);

    /**
     * @brief Positionne le véhicule à l'aide du repère formé par les intersections
     * 
     */
    void setPosByIndex(int x, int y);

    /**
     * @brief Retourne vrai si le véhicule se trouve sur une intersection
     * 
     */
    bool isOnACrossroads() const;

    /**
     * @brief Modifie la destination du vehicule
     * 
     * @param dest 
     * @return * Modifie 
     */
    void setDestination(Infrastructure &dest);

    /**
     * @brief Retourne vrai si le vehicule se trouve dans la zone de service de la destination
     * 
     * @return * Retourne 
     */
    bool isOnTheServiceAreaOf() const;

    /**
     * @brief Retourne le niveau de charge du vehicule
     * 
     * @return * Retourne 
     */
    unsigned int getChargeLevel() const;

    /**
     * @brief Modifie le niveau de charge vers une charge maximale
     * 
     * @param charge_ 
     * @return * Modifie 
     */
    void charge(int charge_);

    /**
     * @brief Retourne vrai si le vehicule doit se charger
     * 
     * @return * Retourne 
     */
    bool needCharge() const;

    /**
     * @brief Retourne les informations du vehiculer
     * 
     * @return * Retourne 
     */
    string getInfo() const;

    /**
     * @brief Gère des déplacements du véhicule
     * 
     */
    void running();

    /**
     * @brief Gère les niveaux critiques de décharges
     * 
     */
    void handleFailSafe();

    /**
     * @brief Calcul le coût de déplacement jusqu'à im
     * 
     * @param im 
     * @return int 
     */
    int costToGoTo(const InfraManager *im);

    /**
     * @brief Obtient le ratio de profitabilité d'un ilot.
     * 
     * @param inf 
     * @return int 
     */
    int getRatioFromInfra(const Infrastructure *inf);

    /**
     * @brief Retourne le spot le plus rentable
     * 
     * @param except 
     * @param max 
     * @return ChargingSpot* 
     */
    ChargingSpot *getWorthiestSpot(ChargingSpot *except[], int max);

    /**
     * @brief Retourne les spots les plus rentables
     * 
     * @param res 
     * @param max 
     */
    void getWorthiestSpots(ChargingSpot *res[], int max);

    /**
     * @brief Gère la recharge
     * 
     */
    void handleCharge();

    /**
     * @brief Permet le maintien des fonctions essentielles du vehicule
     * 
     * @return * Permet 
     */
    void process();

    /**
 * fonctions hérités de Communicator
 * fonctions modifiées
 * */
    void actionOui(const Communicator *Com = nullptr);
    void actionNon(const Communicator *Com = nullptr);
    void actionPrix(const Communicator *, float price = 0);
    void askModif(const Communicator *I, float newPrice);
    void askPrice(const Communicator *I, int charge);
    void Qvaleur(const Communicator *V, float valeur);

    virtual void actionFailed(const Communicator *) override;
};

#endif