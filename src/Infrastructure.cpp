#include "Infrastructure.h"

Infrastructure::Infrastructure(RoadsNetwork *p_rn) : Sprite()
{
	rn = p_rn;
	infraIm.loadFromFile("content/cityscape.png");

	texture.loadFromImage(infraIm);
	this->setTexture(texture);
	this->setScale(rn->customScale, rn->customScale);
	this->setOrigin(256, 256);

	police.loadFromFile("content/police/Roboto-Regular.ttf");
	text.setFont(police);
	text.setCharacterSize(20);
	text.setFillColor(Color::Black);

	updateInfoString();
}

/**
	 *  Met à jour les information de l'infrastructure dans le "Text" associé
	 */
void Infrastructure::updateInfoString()
{
	text.setString("Nb voiture garee : " + to_string(numberOfCars));
}

/** Modifie la position de l'infrastructure de manière brute*/
void Infrastructure::setPos(const float x, const float y)
{
	this->setPosition(x, y);
	text.setPosition(x + 60, y - 20);
}

/** Modifie la position de l'infrastructure en utilisant le repère formé par les routes*/
void Infrastructure::setPosByIndex(int x, int y)
{
	posX = x;
	posY = y;
	this->setPos(rn->posXIndex[x], rn->posYIndex[y]);
	destPointX1 = rn->vRoadsPoints[x][0] + (0.33) * rn->dx;
	destPointY1 = rn->hRoadsPoints[y][1] + rn->dy;

	destPointX2 = destPointX1 + 0.33 * rn->dx;
	destPointY2 = destPointY1 + rn->roadSize;

	arret.setPosition(destPointX1 + rn->roadSize, destPointY1);
	arret.setSize(Vector2f(0.33 * rn->dx, rn->roadSize));
	arret.setFillColor(Color::Red);
}

/** Ajoute une voiture parmis les voitures en charge*/
void Infrastructure::addCar(void)
{
	numberOfCars = numberOfCars + 1;
	updateInfoString();
}

/** Retire une voiture parmis celles en rechargement */
void Infrastructure::deleteCar(void)
{
	numberOfCars = numberOfCars - 1;
	updateInfoString();
}

/** Retourne le nombre de voiture en rechargement*/
unsigned int Infrastructure::getCar(void)
{
	return numberOfCars;
}
