#ifndef PNT_INTERET_H
#define PNT_INTERET_H

#include "global.h"
#include "Infrastructure.h"
#include "RoadsNetwork.h"
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <string.h>
#include <time.h>
using namespace sf;

/**
 * @brief infrastructure de point d'intérêt
 */
class PointInteret : public Infrastructure
{
public:
 /**
  * @brief COnstructeur de point d'intérêt
  * @param p_rn
  */
	PointInteret(RoadsNetwork *p_rn);
 /**
  * @brief Renvoie le type de destination
  * @return string
  */
	virtual string getDestType();
};

#endif