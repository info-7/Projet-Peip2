#ifndef GLOBAL_H
#define GLOBAL_H

#define nbRoadX 10		/**< Nombre de routes horizontales*/
#define nbRoadY nbRoadX /**< Nombre de routes verticales*/

#define screenX 1750 /**< Largeur de fenêtre */
#define screenY 850	 /**< Hauteur de fenêtre */
#define simuSpeed 20
#define frameRate 1000

#define marginRS 0 /**< Marge entre le bord de fenêtre et les routes */

#define nbVoitures 1000
/**< Nombre de voitures dans la simulation */
#define nbPointInteret 50 /**< Nombre de point d'intérêt */
#define nbIlots 20		 /**< Nombre d'îlots de charge */

#define recordFichier true ///mettre les données dans un fichier csv
#define sommeOrigine 1000 * 100 ///somme d'argent en euros au départ de la simulation

#define prixStation 20 /**< Prix de base de l'électricité à la station la plus chère */

#endif