#ifndef INFRA_CPP
#define INFRA_CPP

#include "global.h"
#include "RoadsNetwork.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace sf;
using namespace std;

/**
 * @brief classe définissant une infrastructure
 */
class Infrastructure : public Sprite
{
protected:
    unsigned int numberOfCars = 0; /// Nombre de voitures garées

public:
    RoadsNetwork *rn; /// Pointeur vers le réseau auto-routier
    Image infraIm; /// Image de l'infrastructure
    Texture texture; /// Texture
    Font police; /// Police
    Text text;

    int destPointX1, destPointY1; /**< Points caractérisant l'arrêt .*/
    int destPointX2, destPointY2; /**< Points caractérisant l'arrêt .*/

    int posX = -1, posY = -1; /// Position dans la grille

    RectangleShape arret; /**< Rectangle définissant la zone de service */

    /**
     * @brief Différents types d'infrastructure
     * 
     */
    enum typeInfra
    {
        PointInteret,
        station,
        ilot
    };

    typeInfra type;

    /**
     * @brief constructeur de Infrastructure
     * @param p_rn
     */
    Infrastructure(RoadsNetwork *p_rn);

    /**
     * @brief Met à jour les information de l'infrastructure dans le "Text" associé
     * @return 
     */
    virtual void updateInfoString();

    /**
     * @brief  Modifie la position de l'infrastructure
     * @param x
     * @param y
     * @return (void)
     */
    void setPos(const float x, const float y);

    /**
     * @brief Modifie la position de l'infrastructure en utilisant le repère formé par les routes
     * @param x
     * @param y
     * @return (void)
     */
    void setPosByIndex(int x, int y);

    /**
     * @brief Retourne le type de l'infrastructure
     * 
     * @return string 
     */
    
    virtual string getDestType() = 0;
    
    /**
     * @brief Ajoute une voiture parmis les voitures en charge
     * 
     */
    void addCar(void);

    /**
     * @brief Retire une voiture parmis celles en rechargement 
     * @param void
     * @return (void)
     */
    void deleteCar(void);

    /**
     * @brief Retourne le nombre de voiture en rechargement
     * @param void
     * @return int
     */
    unsigned int getCar(void);
};

#endif