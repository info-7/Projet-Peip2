
#include "Car.h"

Car::Car(RoadsNetwork *p_rn, InfraManager *im) : Sprite(), Communicator()
{
	rn = p_rn;
	this->infraManager = im;
	constantSpeedProcess = ((float)rn->dx) / (((float)3.6) * ((float)frameRate) * ((float)100));
	//Charge l'image d'une voiture
	carImg.loadFromFile("content/arrow.png");
	setColor(Color::White);
	texture.loadFromImage(carImg);
	this->setTexture(texture);
	this->setScale(0.05, 0.05);
	this->setOrigin(256, 256);

	//Charge la police d'écriture
	police.loadFromFile("content/police/Roboto-Regular.ttf");
	text.setFont(police);
	text.setCharacterSize(10);
	text.setFillColor(Color::Black);

	timeParked = time(NULL);

	for (int i = 0; i < 5; i++)
	{
		TabIlot[i].I = nullptr;
		TabIlot[i].prix_negoc = 0;
		TabIlot[i].ilotOK = false;
		TabIlot[i].tries = 0;
	}
}

Car::~Car()
{
}

void Car::setPos(const float x, const float y)
{
	this->setPosition(x, y);
	coordX = ((float)x);
	coordY = ((float)y);
	text.setPosition(x + 15, y - 3);
}

void Car::setPosByIndex(int x, int y)
{
	setPos(rn->vRoadsPoints[x][0] + 0.5 * rn->roadSize, rn->hRoadsPoints[y][1] + 0.5 * rn->roadSize);
	RoadXused = x;
	RoadYused = y;
}

bool Car::isOnACrossroads() const
{
	if (getPosition().x > rn->vRoadsPoints[RoadXused][0] && getPosition().x < rn->vRoadsPoints[RoadXused][2] && getPosition().y > rn->hRoadsPoints[RoadYused][1] && getPosition().y < rn->hRoadsPoints[RoadYused][3])
	{
		return true;
	}
	return false;
}

void Car::setDestination(Infrastructure &dest)
{
	this->dest = &dest;
}

bool Car::isOnTheServiceAreaOf() const

{
	return this->getPosition().x > dest->destPointX1 && this->getPosition().x<dest->destPointX2 &&this->getPosition().y> dest->destPointY1 && this->getPosition().y < dest->destPointY2;
}

unsigned int Car::getChargeLevel() const //retourne le niveau de charge en % * 1000
{
	return chargeLevel;
}

void Car::charge(int charge_) //permet de charge la voiture
{
	chargeLevel += charge_;
}

bool Car::needCharge() const
{
	if (chargeLevel < 25000)
	{
		return true;
	}
	else
	{
		return false;
	}
}

string Car::getInfo() const
{
	return "Destination : " + dest->getDestType() + " \n Niveau de charge : " + to_string((float)chargeLevel / (float)1000);
}

void Car::running()
{
	if (!parked)
	{

		if (!isOnACrossroads() && isOnACrossRoad) /* Condition vérifiée : La voiture a quitté une intersection */
		{
			isOnACrossRoad = false;
			setPosByIndex(RoadXused, RoadYused);
			if (speedY < 0)
			{
				RoadYused--;
			}
			else if (speedY > 0)
			{
				RoadYused += 1;
			}
			if (speedX < 0)
			{
				RoadXused--;
			}
			else if (speedX > 0)
			{
				RoadXused++;
			}
		}
		else if (isOnACrossroads()) /* La voiture est sur une intersection */
		{

			//Gestion de la direction du déplacement
			if (getPosition().y < dest->destPointY1 && getPosition().y < dest->destPointY2)
			{
				speedY = vmax;
				speedX = 0;
				setRotation(90);
			}
			else if (getPosition().y > dest->destPointY1 && getPosition().y > dest->destPointY2)
			{
				speedY = -vmax;
				speedX = 0;
				setRotation(270);
			}
			else if (getPosition().x < dest->destPointX1 && getPosition().x < dest->destPointX2)
			{
				speedX = vmax;
				speedY = 0;
				setRotation(0);
			}
			else if (getPosition().x > dest->destPointX1 && getPosition().x > dest->destPointX2)
			{
				speedX = -vmax;
				speedY = 0;
				setRotation(180);
			}

			//// cout << "On est sur une intersection : " << RoadXused << RoadYused << endl;
			isOnACrossRoad = true;
		}

		//Palier au erreurs d'arrondis à défaut

		coordX += speedX * constantSpeedProcess * simuSpeed;
		coordY += speedY * constantSpeedProcess * simuSpeed;
		setPos(coordX, coordY); //mettre la voiture à la nouvelle position

		//calcul du nouveau niveau de batterie
		chargeLevel -= simuSpeed;
		unsigned int decimalLevel = chargeLevel % 1000;
		text.setString(std::to_string((chargeLevel - decimalLevel) / 1000) + '.' + std::to_string(decimalLevel));
	}
	else if (difftime(time(NULL), timeParked) > waitingTime / simuSpeed)
	{
		dest->deleteCar();
		setDestination(*infraManager->P[rand() % (nbPointInteret)]);
		parked = false;
	}
}

void Car::handleFailSafe()
{
	if (chargeLevel < 20000 && dest->getDestType() != "Station" && !parked)
	{
		setDestination(*infraManager->S);
		infraManager->S->addCarTotheQueue(this, 40000 - chargeLevel);
	}
}

int Car::getRatioFromInfra(const Infrastructure *im)
{
	ChargingSpot *target = (ChargingSpot *)im;
	int targetPrice = target->getPrice();
	int res = 0;
	float qttConsommee = 0.f;
	qttConsommee += abs(RoadXused - target->posX) / rn->dx;
	qttConsommee += abs(RoadYused - target->posY) / rn->dy;
	res = (qttConsommee + (100000 - chargeLevel)) * targetPrice;
	return res;
}

ChargingSpot *Car::getWorthiestSpot(ChargingSpot *except[], int max)
{
	bool finished = false;
	int bestRatio = -1;
	ChargingSpot *worthiest = nullptr;
	Infrastructure *buff = nullptr;

	int dx = 0, dy = 0;
	int rang = 0, cursor = 0;
	int dir = 0;

	while (!finished)
	{

		if (dx > nbRoadX && dy > nbRoadY)
			finished = true;

		//// cout << "dir = " << dir << " /// " << "dx/dy : " << dx << "/" << dy <<endl;

		if (!(RoadXused + dx >= nbRoadX || RoadYused + dy >= nbRoadY || RoadXused + dx < 0 || RoadYused + dy < 0))
		{
			buff = infraManager->getInfra(RoadXused + dx, RoadYused + dy);
		}

		if (buff != nullptr && (buff->getDestType() == "Ilot" || buff->getDestType() == "Station"))
		{
			int ratio = getRatioFromInfra(buff);
			// cout << "Candidat testé : " << buff->getDestType() << "(" << buff->posX << "/" << buff->posY << ")" << " // Ratio calculé : " << ratio << endl;
			bool isAlreadyIn = false;

			for (int i = 0; i < max; i++)
			{
				if (except[i] != nullptr)
				{
					if (except[i] == buff)
					{
						isAlreadyIn = true;
					}
				}
			}

			if (!isAlreadyIn && (bestRatio > ratio || bestRatio == -1))
			{
				bestRatio = ratio;
				worthiest = (ChargingSpot *)buff;
			}
		}

		switch (dir)
		{
		case 0:
			dx++;
			break;
		case 1:
			dy++;
			break;
		case 2:
			dx--;
			break;
		case 3:
			dy--;
			break;
		}
		if (cursor < rang)
			cursor++;
		else
		{
			if (dir == 1 || dir == 3)
				rang++;
			cursor = 0;
			dir++;
			if (dir == 4)
				dir = 0;
		}
	}
	return worthiest;
	// cout << endl
}

void Car::getWorthiestSpots(ChargingSpot *res[], int max)
{
	for (int i = 0; i < max; i++)
	{
		res[i] = getWorthiestSpot(res, i);
	}
}

void Car::handleCharge()
{

	int chargeWanted = 80000 - chargeLevel;

	if (chargeLevel < 50000 && dest->getDestType() != "Ilot" && dest->getDestType() != "Station")
	{

		if (!waitAnswers)
		{
			ChargingSpot *res[5] = {nullptr};
			int max = 5;

			getWorthiestSpots(res, max);

			for (int i = 0; i < 5; i++)
			{
				if (res[i] != nullptr)
					askPrice(res[i], chargeWanted);
			}
			waitAnswers = true;
		}
		else
		{
			bool aChoiceCanBeMade = true;

			for (int i = 0; i < 5; i++)
			{
				// cout << TabIlot[i].I << '\t' << TabIlot[i].ilotOK << '\t' << TabIlot[i].tries << endl;
				if (TabIlot[i].I != nullptr && !TabIlot[i].ilotOK && (TabIlot[i].tries < 3))
					aChoiceCanBeMade = false;
			}

			// cout << "choix ok ? " << aChoiceCanBeMade << endl;

			ChargingSpot *min = nullptr;
			int min_prixNegoc = -1;

			nbDenieds = 0;
			nbRequete = 0;
			nbFailed = 0;
			nbSuccess = 0;

			if (aChoiceCanBeMade)
			{
				for (int i = 0; i < 5; i++)
				{
					if(TabIlot[i].I != nullptr) nbRequete++;
					if(TabIlot[i].I != nullptr && !TabIlot[i].ilotOK && (TabIlot[i].tries < 3)) nbDenieds++;
					if(TabIlot[i].I != nullptr && (TabIlot[i].tries >= 3)) nbFailed++;
					if(TabIlot[i].I != nullptr && TabIlot[i].ilotOK) nbSuccess++;
					if (TabIlot[i].ilotOK && (min == nullptr || (min_prixNegoc > TabIlot[i].prix_negoc)))
					{
						min = TabIlot[i].I;
						min_prixNegoc = TabIlot[i].prix_negoc;
					}
				}
				if (min != nullptr)
				{
					// cout << "Trouvé ! Changement de direction vers : " << min->posX << " / " << min->posY << " | " << min->getDestType() << endl;
					// cout << "Charge actuelle : " << chargeLevel << endl;

					setDestination(*min);
					((ChargingSpot *)dest)->addCarTotheQueue(this, chargeWanted);
					wallet -= min_prixNegoc * chargeWanted/(1000);
					waitAnswers = false;

					for (int i = 0; i < 5; i++)
					{
						TabIlot[i].I = nullptr;
						TabIlot[i].prix_negoc = 0;
						TabIlot[i].ilotOK = false;
						TabIlot[i].tries = 0;
					}
				}
			}
		}
	}
	if (isOnTheServiceAreaOf() && !parked)
	{
		dest->addCar();
		parked = true;
		if (dest->getDestType() == "Station" || dest->getDestType() == "Ilot")
		{

			int chargeDebug = ((ChargingSpot *)dest)->getCharge(this);
			charge(chargeDebug);
		}
		time(&timeParked);
	}
}

void Car::process()
{
	handleFailSafe();
	processQueue();
	handleCharge();
	running();
	if(coordX < 0 || coordX > screenX || coordY < 0 || coordY > screenY) setPosByIndex(rand() % (nbRoadY - 1), rand() % (nbRoadX - 1));
}

void Car::actionOui(const Communicator *il)
{
	int i = 0;
	
	while (TabIlot[i].I != nullptr && (TabIlot[i].I != (ChargingSpot *)il) and (i < 5))
	{
		i++;
	}
	if (i < 5 && TabIlot[i].I != nullptr)
	{
		TabIlot[i].ilotOK = true;
	}
}

void Car::actionNon(const Communicator *il)
{
	int i = 0;
	while ((TabIlot[i].I != (ChargingSpot *)il) and (i < 5))
	{
		i = i + 1;
	}
	if ((i < 5) and (TabIlot[i].tries < 3))
		actionPrix(il, TabIlot[i].I->getPrice());
	//	// cout << "action effectuées" << endl;
}

void Car::actionPrix(const Communicator *I, float prix)
{
	//// cout << "actionPrix " << prix << endl;
	int i = 0;
	float newprice = 0;
	while ((TabIlot[i].I != (ChargingSpot *)I) and (i < 5))
	{
		i++;
	}
	//// cout << TabIlot[i].tries;
	switch (TabIlot[i].tries)
	{
	case 0:
		newprice = (101 - rand() % 50) * prix / 100;
		// cout << "case 0" << endl;
		break;
	case 1:
		newprice = 0.85 * prix;
		// cout << "case 1" << endl;
		break;
	case 2:
		newprice = prix;
		// cout << "case 2" << endl;
		break;
		//default:
		// cout << "case 3 : transaction échouée" << endl;
	}
	askModif(I, newprice);
}

void Car::askModif(const Communicator *I, float newPrice) ///fonction d'envoi/reception pour les voitures
{
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[0] = true; //Q=true
	R.spec[4] = true; //V=true
	R.valeur = newPrice;
	R.charge = 31500 - chargeLevel;
	int i = 0;
	while ((TabIlot[i].I != (ChargingSpot *)I) and (i < 5))
	{
		i++;
	}
	TabIlot[i].tries = TabIlot[i].tries + 1;
	TabIlot[i].prix_negoc = newPrice;
	// cout << "askModif " << newPrice << endl;
	send(I);
}

void Car::askPrice(const Communicator *I, int charge_) ///fonction d'envoi/reception pour les voitures
{
	for (unsigned int i = 0; i < 5; i++)
	{
		R.spec[i] = false;
	}
	R.spec[0] = true; //Q=true
	int i = 0;
	while ((TabIlot[i].I != nullptr) and (i < 5))
	{
		i++;
	}
	R.charge = charge_;
	// cout << i << endl;
	TabIlot[i].I = (ChargingSpot *)I;
	send(I);
}

void Car::Qvaleur(const Communicator *V, float valeur) {}

void Car::actionFailed(const Communicator *I)
{
	int i = 0;
	while ((TabIlot[i].I != (ChargingSpot *)I) and (i < 5))
	{
		i++;
	}
	TabIlot[i].ilotOK = false;
	TabIlot[i].tries = 3;
};