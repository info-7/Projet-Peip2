#ifndef CUSTOM_MAIN_WINDOW_H
#define CUSTOM_MAIN_WINDOW_H
#include "imgui.h"
#include "imgui-SFML.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include "Car.h"
#include "Ilot.h"
#include "Pnt-interet.h"
#include "Road.h"
#include "RoadsNetwork.h"
#include "Station.h"
#include "global.h"
#include "Infrastructure_Manager.h"
#include <SFML/Graphics.hpp>
#include <algorithm>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <string.h>
#include <fstream>
#include <filesystem>


using namespace std;
using namespace sf;

class CustomMW : public RenderWindow
{
	protected:

	public:
	InfraManager* infMan; /// Le gestionnaire d'infrastructure
	RoadsNetwork* rn; /// Le réseau auto-routier
	Car** voiture; /// Les véhicules
	bool initOK = false; /// Initialisé ?
	sf::Clock deltaClock; /// ImGui ?
	sf::Time dureeProg, tempsEcoule; /// Sauvegarde fichier ?
	

	sf::Color bgColor; /// Couleur de fond
	char windowTitle[255] = "ImGui + SFML = <3";
	float color[3] = {0.f, 0.f, 0.f};
	

	CustomMW(VideoMode, String);
	
	/**
	 * @brief Initialisation
	 * 
	 */
	void init();
	~CustomMW();

	/**
	 * @brief Gère l'affichage de la fenêtre
	 * 
	 */
	void handleWindow();
 /**
  * @brief écrit les statistiques de consommation en fonction du temps
  * @return bool, opération fructueuse 
  */
	bool ecrireStats();
};

#endif