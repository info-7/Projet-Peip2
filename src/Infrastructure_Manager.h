#ifndef INFRA_MAN_H
#define INFRA_MAN_H

#include "Infrastructure.h"
#include "Pnt-interet.h"
#include "Ilot.h"
#include "Station.h"
#include "RoadsNetwork.h"
#include <assert.h>

/**
 * @brief Classe permettant de gérer les infrastructures
 */
class InfraManager
{
    protected:
    int t_dimX, t_dimY;
    

    public:
    Infrastructure*** infras; /// Les infrastructures
    vector<Ilot*> allIlots; ///tableau d'ilots triés par ordre croissant de prix
	Station* S; /// La Stations
	PointInteret** P; /// les points d'intérêts

    /**
     * @brief Construteur par défaut
     */
    InfraManager();
    /**
     * @brief Constructeur permettant de gérer les infrastructures dans la carte
     * @param rn
     */
    InfraManager(RoadsNetwork* rn);
    /**
     * @brief déstructeur de la classe
     */
    ~InfraManager();

    /**
     * @brief Récupère l'infrastructure aux coordonnées X Y
     * @param X
     * @param Y
     * @return Pointeur sur l'infrastructure
     */
    Infrastructure* getInfra(int X, int Y);
    /**
     * @brief Définie l'infrrastructure aux coordonnées X Y
     * @param X
     * @param Y
     * @param buff
     * @return (void)
     */
    void setInfra(int X, int Y, Infrastructure* buff);
    /**
     * @brief renvoie si la position renseignée est utilisée ou pas
     * @param X
     * @param Y
     * @return bool
     */
    bool isPosUsed(int X, int Y);
    /**
     * @brief fonction de débug
     * @param rn
     * @return (void)
     */
    void debug(RoadsNetwork* rn);
    /**
     * @brief met à jour l'écriture à côté des îlots et leur niveau de charge
     * @return (void)
     */
    void handleChargingSpotCharge();
};

#endif