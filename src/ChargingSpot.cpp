#include "ChargingSpot.h"

ChargingSpot::ChargingSpot(RoadsNetwork *p_rn) : Infrastructure(p_rn), Communicator()
{

	carWillingGetCharge = new Car *[nbVoitures];
	for (int i = 0; i < nbVoitures; i++)
		carWillingGetCharge[i] = nullptr;
	energySold = new int[nbVoitures];
}

ChargingSpot::~ChargingSpot()
{
	delete[] carWillingGetCharge;
	delete[] energySold;
}

/**
	*	Voici la magnifique setRandomCharge() !
	*/
void ChargingSpot::setRandomCharge()
{
	chargeLevel = (unsigned int)(rand() % 100001);
}

/** Met à jour le prix du point de charge*/
void ChargingSpot::setPrice(const int p)
{
	if (p < 0)
		price = 0;
	else
		price = p;
	updateInfoString();
}

void ChargingSpot::setCharge(int charge_)
{
	chargeLevel = charge_;
	updateInfoString();
}

/** Retourne le prix pratiqué par le point de charge*/
unsigned int ChargingSpot::getPrice(void) const
{
	return price;
}

int ChargingSpot::getCharge(Car *c)
{
	int energySoldBuff = -1;
	for (int i = 0; i < nbVoitures; i++)
	{
		if (carWillingGetCharge[i] == c)
		{
			energySoldBuff = energySold[i];

			energySold[i] = 0;
			carWillingGetCharge[i] = nullptr;
		}
	}
	return energySoldBuff;
}

bool ChargingSpot::addCarTotheQueue(Car *c, int chargeSold)
{

	for (int i = 0; i < nbVoitures; i++)
	{
		if (carWillingGetCharge[i] == nullptr && chargeLevel - chargeSold >= 0)
		{

			chargeLevel -= chargeSold;
			if (getDestType() != "Station")
				setPrice(prixStation + 5 - (float)0.2 * ((float)chargeLevel) / ((float)1000));

			energySold[i] = chargeSold;
			carWillingGetCharge[i] = c;
			return true;
		}
	}
	return false;
}

//A modifier pour ne pas accepter en cas de charge trop importante
void ChargingSpot::Qvaleur(const Communicator *V, float valeur, int charge)
{
	R.failed = false;
	// cout<<"Qvaleur "<<chargeLevel<<' '<< valeur<<' '<<price<<endl;
	if (price < prixStation / 2)
	{
		sendYes(V);
	}
	else if (chargeLevel - charge < 0)
	{
		R.failed = true;
		sendNo(V);
	}
	else if (chargeLevel > 50000)
	{
		if (valeur >= this->price * 0.8)
			sendYes(V);
		else
			sendNo(V);
	}
	else
	{
		// cout << "Qvaleur " << chargeLevel << ' ' << valeur << ' ' << price << endl;
		if (chargeLevel > 50000)
		{
			if (valeur >= this->price * 0.8)
				sendYes(V);
			else
				sendNo(V);
		}
		else
		{
			if (valeur >= this->price * 0.90)
				sendYes(V);
			else
				sendNo(V);
		}
	}
}

void ChargingSpot::actionPrix(const Communicator *, float price)
{
	// cout << "ilot actionPrix" << endl;
}

void ChargingSpot::actionNon(const Communicator *) {}

void ChargingSpot::actionOui(const Communicator *) {}

void ChargingSpot::sendPrice(const Communicator *V, unsigned int prix, unsigned int charge)
{
	if (chargeLevel < charge)
		sendNo(V);
	else
	{
		for (unsigned int i = 0; i < 5; i++)
		{
			R.spec[i] = false;
		}
		R.spec[1] = true; //R=true
		R.spec[4] = true; //V=true
		R.valeur = prix;
		send(V);
	}
}