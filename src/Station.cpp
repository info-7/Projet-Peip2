#include "Station.h"

	Station::Station(RoadsNetwork *p_rn) :
		ChargingSpot(p_rn)
	{
		setPrice(prixStation);
		infraIm.loadFromFile("content/gas-station.png");
		texture.loadFromImage(infraIm);
		this->setTexture(texture);
		this->setOrigin(256, 256);
		chargeLevel = 2100000000;
		calculposI();
		setPosition(100, 100);
		type = typeInfra::station;

		updateInfoString();
	}

	void Station::updateInfoString()
	{
		text.setString("Prix :" + to_string((int) getPrice()) + '\n' + "voitures en charges : " + to_string(getCar()));
	}

	/** Place la station au centre de la carte*/
	void Station::calculposI()
	
	{
		if (nbRoadX > 2)
		{
			if ((nbRoadX % 2 == 0) && (nbRoadX == nbRoadY))
			{
				posI = (nbRoadX / 2) - 1;
			}
			else
			{
				posI = ((nbRoadX - 1) / 2) - 1;
			}
		}
		else
		{
			posI = 0;
		}
	}

string Station::getDestType(){
	return "Station";
}