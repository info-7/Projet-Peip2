#include "imgui.h"
#include "imgui-SFML.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include "CustomSecondWindow.h"
#include "CustomMainWindow.h"
#include <SFML/Graphics.hpp>
#include <math.h>
#include <stdlib.h>
#include <time.h>
using namespace sf;
using namespace std;

int main()
{
	srand(time(NULL));
	// création de la fenêtre + limitation des fps pour les perfs
	CustomMW window(sf::VideoMode(screenX, screenY), "Projet Peip2");
	
	// on fait tourner le programme tant que la fenêtre n'a pas été fermée
	
	while (window.isOpen() /*&& stat.isOpen()*/)
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			ImGui::SFML::ProcessEvent(event);
			// fermeture de la fenêtre lorsque l'utilisateur le souhaite
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.handleWindow();

		
	}
	ImGui::SFML::Shutdown();
	return 0;
}