#ifndef ROAD_H
#define ROAD_H

#include <SFML/Graphics.hpp>

using namespace sf;

class Road : public RectangleShape
{
private:
	/* data */
public:
	Road();

	/**
	 * @brief Renseigne les caractéristiques de la routes
	 * 
	 * @param posX 
	 * @param posY 
	 * @param size 
	 */
	void setCaracts(unsigned int posX, unsigned int posY, Vector2f size);
	~Road();
};

#endif