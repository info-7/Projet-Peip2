#include "Ilot.h"

Ilot::Ilot(RoadsNetwork *p_rn) :
	ChargingSpot(p_rn)
{
	
	this->setRandomCharge();
	setPrice(prixStation + 5 - (float) 0.2 * ((float) chargeLevel) / ((float) 1000));
	// cout << "Prix enregistré" << prixStation + 5 - (float) 0.2 * ((float) chargeLevel) / ((float) 100000) << endl;
	infraIm.loadFromFile("content/cityscape.png");

	type = typeInfra::ilot;

	updateInfoString();
}

void Ilot::updateInfoString()
{
	updatePrice();
	text.setString("Energie disponible : " + std::to_string(chargeLevel) + '\n' + "prix : " + std::to_string(price) + '\n' + "voitures en charges : " + std::to_string(numberOfCars));
}

string Ilot::getDestType(){
	return "Ilot";
}

void Ilot::updatePrice(){
	if(prixStation + 5 - (float) 0.2 * ((float) chargeLevel) / ((float) 1000) > 0)price =  prixStation + 5 - (float) 0.2 * ((float) chargeLevel) / ((float) 1000);
	else price = 0;
}