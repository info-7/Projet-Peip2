#include "Infrastructure_Manager.h"

InfraManager::InfraManager(RoadsNetwork* rn)
{

		t_dimX = nbRoadY - 1; t_dimY = nbRoadX - 1;
    infras = new Infrastructure** [t_dimX];

    for(int i = 0; i < t_dimX; i++){
        infras[i] = new Infrastructure* [t_dimY];
    }

    	    //Initialisation du tableau de présence sur Index
	for (int i = 0; i < t_dimX; i++)
	{
		for (int x = 0; x < t_dimY; x++)
		{
			infras[i][x] = nullptr;
		}
	}

	S = new Station(rn);
    S->rn = rn;
	setInfra(S->posI, S->posI, S);
	S->setPosByIndex(S->posI, S->posI);
		P = new PointInteret *[nbPointInteret];
	for (int i = 0; i < nbPointInteret; i++)
	{
		PointInteret *buff = new PointInteret(rn);
		P[i] = buff;
        P[i]->rn = rn;

		int indexX, indexY;
		do
		{
			indexX = rand() % (t_dimX);
			indexY = rand() % (t_dimY);
		} while (isPosUsed(indexX,indexY));
        P[i]->rn = rn;
		P[i]->setPosByIndex(indexX, indexY);
		setInfra(indexX, indexY, P[i]);
	}

	allIlots.resize(nbIlots);
	for (int i = 0; i < nbIlots; i++)
	{
		allIlots[i] = new Ilot(rn);
        allIlots[i]->rn = rn;
		int indexX, indexY;
		do
		{
			indexX = rand() % (t_dimX);
			indexY = rand() % (t_dimY);
		} while (isPosUsed(indexX,indexY));
        
		allIlots[i]->setPosByIndex(indexX, indexY);
		setInfra(indexX, indexY, allIlots[i]);
	}

}

InfraManager::~InfraManager()
{
    for(int i = 0; i < t_dimX; i++){
        delete [] infras[i];
    }

    delete [] infras;
}

Infrastructure * InfraManager::getInfra(int X, int Y)
{
	if( X >= t_dimX|| Y >= t_dimY) return nullptr;
    return infras[X][Y];
}

void InfraManager::setInfra(int X, int Y, Infrastructure *buff)
{
    infras[X][Y] = buff;
}

bool InfraManager::isPosUsed(int X, int Y){
    if(infras[X][Y] == nullptr) return false;
    else return true;
}

void InfraManager::handleChargingSpotCharge(){
	for(int i = 0; i < nbIlots; i++){
		allIlots[i]->chargeLevel += 10;
		allIlots[i]->updateInfoString();
	}
}