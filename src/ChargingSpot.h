#ifndef CHARGING_SPOT_CPP
#define CHARGING_SPOT_CPP

#include "global.h"
#include "Communicator.h"
#include "Infrastructure.h"
#include "RoadsNetwork.h"
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace sf;

class Car;

class ChargingSpot : public Infrastructure, public Communicator
{
protected:
    unsigned int price; /**< Prix de l'électricité*/
public:
    int chargeLevel;           /**< Charge disponible pour une voiture */
    Car **carWillingGetCharge; /// les véhicule voulant se faire charger
    int *energySold;           ///Les énergies vendues

    ChargingSpot(RoadsNetwork *p_rn);
    ~ChargingSpot();

    /**
	*	@briefla magnifique setRandomCharge() !
	*/
    void setRandomCharge();
    /**
     * @brief Met à jour le prix du point de charge
     * 
     */
    void setPrice(const int p);
    /**
     * @brief Fixe la charge
     * 
     * @param charge_ 
     */
    void setCharge(int charge_);
    /**
     * @brief Retourne le prix pratiqué par le point de charge
     * 
     */
    unsigned int getPrice(void) const;

    /**
     * @brief Met à jour la chaîne de caractère informative
     * 
     */
    virtual void updateInfoString() = 0;

    /**
     * @brief Retourne la charge réservée pour c
     * 
     * @param c 
     * @return int 
     */
    int getCharge(Car *c);

    /**
     * @brief Ajoute c à la file d'attente
     * 
     * @param c 
     * @param chargeSold 
     * @return true 
     * @return false 
     */
    virtual bool addCarTotheQueue(Car *c, int chargeSold);

    /**
 * fonctions hérités de Communicator
 * fonctions modifiées
 * */

    void actionOui(const Communicator *);
    void actionNon(const Communicator *);
    void actionPrix(const Communicator *, float price = 0);
    void Qvaleur(const Communicator *V, float valeur, int charge);
    void sendPrice(const Communicator *V, unsigned int prix, unsigned int charge = -1) override;
};

#endif