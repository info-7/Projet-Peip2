#include "CustomMainWindow.h"
#include <assert.h>
CustomMW::CustomMW(VideoMode, String) : RenderWindow(sf::VideoMode(screenX, screenY), "Projet Peip2", sf::Style::Close)
{
	//setFramerateLimit(frameRate);
}

void CustomMW::init()
{
	//setVerticalSyncEnabled(true);
	ImGui::SFML::Init(*this);
	resetGLStates();

	rn = new RoadsNetwork(this);

	infMan = new InfraManager(rn);

	voiture = new Car *[nbVoitures];
	for (int i = 0; i < nbVoitures; i++)
	{
		voiture[i] = nullptr;
		voiture[i] = new Car(rn, infMan);
		assert(voiture[i] != nullptr);

		voiture[i]->infraManager = infMan;
		voiture[i]->parked = false;
		voiture[i]->waitAnswers = false;
		(voiture[i])->setDestination(*infMan->P[rand() % (nbPointInteret - 1)]);
		(voiture[i])->setPosByIndex(rand() % (nbRoadY - 1), rand() % (nbRoadX - 1));
	}

	/*
		Placement des objets
	*/
}
CustomMW::~CustomMW()
{
	delete rn;
	delete infMan->S;

	for (int i = 0; i < nbVoitures; i++)
	{
		if (voiture[i] != nullptr)
			delete voiture[i];
	}
	delete[] voiture;

	for (int i = 0; i < nbPointInteret - 1; i++)
	{
		if (infMan->P[i] != nullptr)
			delete infMan->P[i];
	}
	delete[] infMan->P;

	for (int i = 0; i < nbIlots; i++)
	{
		if (infMan->allIlots[i] != nullptr)
			delete infMan->allIlots[i];
	}
	delete infMan;
}

using namespace ImGui;

time_t startTime = time(0);
int prixMoyenIlot = 0, minIlot = -1, maxIlot = -1;

void ilotPrintIMGUI(int nb, int charge, int price)
{
	std::string nb_st;
	nb_st.append(to_string(nb));

	string charge_st;
	charge_st.append(to_string(charge));
	string price_st;
	price_st.append(to_string(price));

	if (TreeNode((nb_st + " \0").c_str()))
	{
		ImGui::Text(("Charge : " + charge_st).c_str());

		ImGui::Text(("Prix : " + price_st).c_str());
		ImGui::TreePop();
	}
}

void voiturePrintIMGUI(Car *v, int nb, int charge = 0, int wallet = 0)
{
	std::string nb_st;
	nb_st.append(to_string(nb));

	if (ImGui::TreeNode(to_string(nb).c_str()))
	{
		if (ImGui::Checkbox("Colorer ?", &v->colorized))
		{
			if (v->colorized)
				v->carImg.loadFromFile("content/blueArrow.png");
			else
				v->carImg.loadFromFile("content/arrow.png");

			v->texture.loadFromImage(v->carImg);
			v->setTexture(v->texture);
		}

		ImGui::Text(("Charge : " + to_string(v->getChargeLevel())).c_str());

		string wally = "Wallet : " + to_string(v->wallet / 100);
		ImGui::Text(wally.c_str());

		Infrastructure *p = v->dest;
		int Xdest = 0, Ydest = 0;
		if (p != nullptr)
		{
			Xdest = p->posX;
			Ydest = p->posY;
		}
		ImGui::Text(("Destination : (" + to_string(Xdest) + "/" + to_string(Ydest) + ")").c_str());

		string debug = "Nombre de demandes effectuée : " + to_string(v->nbRequete) + 
		"\n Nombre de rejet : " + to_string(v->nbDenieds) + 
		"\n Nombre d'echecs comm : " + to_string(v->nbFailed) +
		"\n Nombre de succès : " + to_string(v->nbSuccess);
		ImGui::Text(debug.c_str());
		ImGui::TreePop();
	}
}

/**
 * @brief Calcul de dépense
 * 
 */

int oldAmount = sommeOrigine * nbVoitures, newAmount = 0;

int calculAVGWithdraw()
{
	return abs(oldAmount - newAmount) / (100 * nbVoitures);
}

void CustomMW::handleWindow()
{
	// on traite tous les évènements de la fenêtre qui ont été générés depuis la dernière itération de la boucle

	//Début + Clear de la fenêtre en Blanc

	if (!initOK)
	{
		initOK = true;
		init();
		dureeProg = deltaClock.restart();
		dureeProg -= dureeProg;
		tempsEcoule = dureeProg;

		if (recordFichier)
		{
			ofstream fluxFile("content/stats.csv", ios::app);
			if (fluxFile)
			{
				int avgWallet = 0;
				int avgCharge = 0;
				int avgEnergy = 0;
				int avgPrice = 0;
				for (int i = 0; i < nbVoitures; i++)
				{
					avgWallet += voiture[i]->wallet;
					avgCharge += voiture[i]->getChargeLevel();
				}
				avgWallet = avgWallet / nbVoitures;
				avgCharge = avgCharge / nbVoitures;
				for (int i = 0; i < nbIlots; i++)
				{
					avgEnergy += infMan->allIlots[i]->chargeLevel;
					avgPrice += infMan->allIlots[i]->getPrice();
				}
				avgEnergy = avgEnergy / nbIlots;
				avgPrice = avgPrice / nbIlots;

				fluxFile << "temps_Simulation"
						 << ", "
						 << "Argent_voitures"
						 << ", "
						 << "chargeVoitures"
						 << ", "
						 << "energieIlots"
						 << ", "
						 << "prixIlots" << endl;
				fluxFile << 0 << ", " << avgWallet << ", " << avgCharge << ", " << avgEnergy << ", " << avgPrice << endl;
				fluxFile.close();
			}
			else
			{
				// cout << "ERREUR: Impossible d'ouvrir le fichier content/stats.csv" << endl;
			}
		}
	}
	else
	{
		//Netoyage de l'écran
		clear(sf::Color::White);

		//Création d'une fenêtre
		ImGui::SFML::Update(*this, deltaClock.restart());
		ImGui::Begin("Informations"); // begin window
		//ImGui::SetWindowSize(ImVec2(300, 300));

		ImGui::Columns(2);

		ImGui::Text("Ilots");
		ImGui::NextColumn();
		ImGui::Text("Voitres");
		Separator();
		ImGui::NextColumn();

		rn->buildRoads();

		//Dessin des Infrastructures

		draw(*infMan->S);
		//draw(infMan->S->text);
		draw(infMan->S->arret);

		for (int i = 0; i < nbPointInteret; i++)
		{
			draw(*infMan->P[i]);
			draw(infMan->P[i]->arret);
			//draw(infMan->P[i]->text);
		}

		prixMoyenIlot = 0;
		minIlot = -1;
		maxIlot = -1;
		for (int j = 0; j < nbIlots; j++)
		{
			infMan->allIlots[j]->processQueue(infMan->allIlots[j]->getPrice());
			draw(*infMan->allIlots[j]);
			//draw(infMan->allIlots[j]->text);
			draw(infMan->allIlots[j]->arret);
			ilotPrintIMGUI(j, infMan->allIlots[j]->chargeLevel, infMan->allIlots[j]->getPrice());
			prixMoyenIlot += infMan->allIlots[j]->getPrice();
			if(minIlot == -1 || minIlot > infMan->allIlots[j]->getPrice()) minIlot = infMan->allIlots[j]->getPrice();
			if(maxIlot == -1 || maxIlot < infMan->allIlots[j]->getPrice()) maxIlot = infMan->allIlots[j]->getPrice();
		}
		prixMoyenIlot /= nbIlots;

		infMan->S->processQueue(infMan->S->getPrice());
		ImGui::NextColumn();

		newAmount = 0;
		for (int i = 0; i < nbVoitures; i++)
		{
			voiture[i]->process();
			if (!voiture[i]->parked)
			{
				draw(*voiture[i]);
				draw((voiture[i])->text);
			}
			voiturePrintIMGUI(voiture[i], i, voiture[i]->getChargeLevel());
			newAmount += voiture[i]->wallet;
		}

		//Charge partiel des ilots

		infMan->handleChargingSpotCharge();

		std::sort(infMan->allIlots.begin(), infMan->allIlots.end(), [](Ilot *a, Ilot *b) { return a->getPrice() < b->getPrice(); }); ///tri les ilots du moins cher au plus cher dans le vector de ilots

		ImGui::End(); // end window

		ImGui::Begin("Statistiques");

		string res = string("Dépense moyenne des véhicules : ") + to_string(calculAVGWithdraw()).c_str();
		ImGui::Text(res.c_str());

		int tempsEcoule = (int)difftime(time(0), startTime) * simuSpeed;

		int sec = -1, min = -1;

		if (tempsEcoule >= 60)
		{
			sec = tempsEcoule % 60;
			min = tempsEcoule / 60;
		}
		string elapseTime;
		if (min == -1)
			elapseTime = "Durée simulation : " + to_string(tempsEcoule) + "s";
		else
		{
			elapseTime = "Durée simulation : " + to_string(min) + " min " + to_string(sec) + " sec ";
		}
		ImGui::Text(elapseTime.c_str());

		string prixMoyIlot = "Prix moyen par ilot : " + to_string(prixMoyenIlot) +
		"\n Prix Max : " + to_string(maxIlot) + 
		"\n Prix Min : "+ to_string(minIlot);
		ImGui::Text(prixMoyIlot.c_str());

		ImGui::End(); // end window

		ImGui::SFML::Render(*this);
		display();
		ecrireStats();
	}
}

bool CustomMW::ecrireStats()
{
	if (recordFichier)
	{
		Time ecartTemps;
		ecartTemps += deltaClock.restart();
		tempsEcoule += ecartTemps;
		dureeProg += ecartTemps;
		if (tempsEcoule.asMilliseconds() >= 5000 / simuSpeed)
		{
			ofstream fluxData("content/stats.csv", ios::app);
			if (fluxData)
			{
				int avgWallet = 0;
				int avgCharge = 0;
				int avgEnergy = 0;
				int avgPrice = 0;
				for (int i = 0; i < nbVoitures; i++)
				{
					avgWallet += voiture[i]->wallet;
					avgCharge += voiture[i]->getChargeLevel();
				}
				avgWallet = avgWallet / nbVoitures;
				avgCharge = avgCharge / nbVoitures;
				for (int i = 0; i < nbIlots; i++)
				{
					avgEnergy += infMan->allIlots[i]->chargeLevel;
					avgPrice += infMan->allIlots[i]->getPrice();
				}
				avgEnergy = avgEnergy / nbIlots;
				avgPrice = avgPrice / nbIlots;

				fluxData << dureeProg.asSeconds()*simuSpeed << ", " << avgWallet << ", " << avgCharge << ", " << avgEnergy << ", " << avgPrice << endl;
				tempsEcoule -= tempsEcoule;
				return true;
			}
			else
			{
				// cout << "ERREUR: Impossible d'ouvrir le fichier content/stats.csv" << endl;
				return false;
			}
		}
		else
			return true;
	}
	else
		return true;
}