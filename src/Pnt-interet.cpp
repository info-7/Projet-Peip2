#include "Pnt-interet.h"

PointInteret::PointInteret(RoadsNetwork *p_rn) :
		Infrastructure(p_rn)
	{
		infraIm.loadFromFile("content/map-point.png");
		texture.loadFromImage(infraIm);
		this->setScale(((float)1.1) * rn->customScale, ((float)1.1) * rn->customScale);
		type = typeInfra::PointInteret;
	}

string PointInteret::getDestType(){
	return "Pnt-Interet";
}