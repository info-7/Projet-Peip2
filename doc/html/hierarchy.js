var hierarchy =
[
    [ "Communicator", "classCommunicator.html", null ],
    [ "RectangleShape", null, [
      [ "Road", "classRoad.html", null ]
    ] ],
    [ "RenderWindow", null, [
      [ "CustomMW", "classCustomMW.html", null ],
      [ "CustomSW", "classCustomSW.html", null ]
    ] ],
    [ "Communicator::requete", "structCommunicator_1_1requete.html", null ],
    [ "RoadsNetwork", "classRoadsNetwork.html", null ],
    [ "Sprite", null, [
      [ "Car", "classCar.html", null ],
      [ "Infrastructure", "classInfrastructure.html", [
        [ "ChargingSpot", "classChargingSpot.html", [
          [ "Ilot", "classIlot.html", null ],
          [ "Station", "classStation.html", null ]
        ] ],
        [ "PointInteret", "classPointInteret.html", null ]
      ] ]
    ] ]
];