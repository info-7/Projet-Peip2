var classRoadsNetwork =
[
    [ "RoadsNetwork", "classRoadsNetwork.html#a78c85ff990043afac1887effa73e89d4", null ],
    [ "buildRoads", "classRoadsNetwork.html#a3335c2ca03667db31c682cf829120262", null ],
    [ "customScale", "classRoadsNetwork.html#a0fa840b28ab5568ee21129535362c4ba", null ],
    [ "dx", "classRoadsNetwork.html#a112c5dc96c3bf59bd80f25943e729159", null ],
    [ "dy", "classRoadsNetwork.html#a7fa236a0618877ab0a51714212c0bc04", null ],
    [ "hRoadsPoints", "classRoadsNetwork.html#a344db52bb86ea499386d02b6c58d61a7", null ],
    [ "isPosUsed", "classRoadsNetwork.html#a5725e2498f97a8b2836d41cd9f695499", null ],
    [ "posXIndex", "classRoadsNetwork.html#a8df47436f9f90787896659375ffb0a09", null ],
    [ "posYIndex", "classRoadsNetwork.html#acdf67b50de6efccdea465eb88584b08c", null ],
    [ "roadSize", "classRoadsNetwork.html#a5a7117cc487dd9acf4522056aa283572", null ],
    [ "vRoadsPoints", "classRoadsNetwork.html#ad8c53ddf2fc930965e3d86ac10c2aba5", null ],
    [ "window", "classRoadsNetwork.html#a32cad55acf4629652638d2fe6536780f", null ]
];