var classcommunicator =
[
    [ "requete", "structcommunicator_1_1requete.html", "structcommunicator_1_1requete" ],
    [ "communicator", "classcommunicator.html#a94acc8df9ead5a56ed33084592587531", null ],
    [ "~communicator", "classcommunicator.html#a1998965fe7335d59348450b0824c4fb3", null ],
    [ "actionNon", "classcommunicator.html#a2668c5393204dd78ad51cbe0a34224b3", null ],
    [ "actionOui", "classcommunicator.html#a53a47bc10e3a90c57e5ed235268585a9", null ],
    [ "actionPrix", "classcommunicator.html#a59bc70faefaa0619cac414a34096114f", null ],
    [ "askModif", "classcommunicator.html#ace01e4034febe88f418c8828b33a15ba", null ],
    [ "askPrice", "classcommunicator.html#add221c72fe6b5337303af4dd547b8c1f", null ],
    [ "processQueue", "classcommunicator.html#aa52adcf10d0f1d8a141e980b2a6a2b3a", null ],
    [ "Qvaleur", "classcommunicator.html#a48fc1496585accce1642989a99a95f41", null ],
    [ "send", "classcommunicator.html#a5045949f2edc19ca83103d7039ff4339", null ],
    [ "sendNo", "classcommunicator.html#a30b62f28990d8da0d07061c668d7f8df", null ],
    [ "sendPrice", "classcommunicator.html#a5d83b96a6aafb1806442b969a65d4da5", null ],
    [ "sendYes", "classcommunicator.html#a8cf13a148807cdd78817e8509c5565fc", null ],
    [ "buff", "classcommunicator.html#a3ded8aa2fb887c02f1637520f6193d45", null ],
    [ "R", "classcommunicator.html#ab9fbd1b4d427737db4ac19bcb9ec9a95", null ],
    [ "valeur", "classcommunicator.html#a3d7b2a52abea0248b6219e1d407478a4", null ]
];