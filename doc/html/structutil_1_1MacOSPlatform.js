var structutil_1_1MacOSPlatform =
[
    [ "MacOSPlatform", "structutil_1_1MacOSPlatform.html#a5c4d42a97776bf5dc6160a12c28c7493", null ],
    [ "getRefreshRate", "structutil_1_1MacOSPlatform.html#a7eb12177a178bad9548a1a6667f5e31f", null ],
    [ "getScreenScalingFactor", "structutil_1_1MacOSPlatform.html#a7a787aed8459e8bb57b413f90c3bbe1c", null ],
    [ "setIcon", "structutil_1_1MacOSPlatform.html#af6516cc3cc1a57ed57d06c95a2e9647d", null ],
    [ "toggleFullscreen", "structutil_1_1MacOSPlatform.html#a6f32f5a3b661222ed17fc7012f659be1", null ]
];