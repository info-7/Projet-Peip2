var namespaceutil =
[
    [ "IPlatform", "structutil_1_1IPlatform.html", "structutil_1_1IPlatform" ],
    [ "LinuxPlatform", "structutil_1_1LinuxPlatform.html", "structutil_1_1LinuxPlatform" ],
    [ "MacOSPlatform", "structutil_1_1MacOSPlatform.html", "structutil_1_1MacOSPlatform" ],
    [ "WindowsPlatform", "structutil_1_1WindowsPlatform.html", "structutil_1_1WindowsPlatform" ]
];