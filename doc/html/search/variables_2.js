var searchData=
[
  ['carimg_236',['carImg',['../classCar.html#a2c5031183cd5c26ca484ea3de308f17b',1,'Car']]],
  ['chargelevel_237',['chargeLevel',['../classCar.html#a3b9f7626aa049534d42e4f8a3129b119',1,'Car::chargeLevel()'],['../classChargingSpot.html#a2b494fd1951766f6674d6063b80921f7',1,'ChargingSpot::chargeLevel()']]],
  ['constantspeedprocess_238',['constantSpeedProcess',['../classCar.html#a7abf80ef827bfadc6e826144b308ea4c',1,'Car']]],
  ['coordx_239',['coordX',['../classCar.html#a045025a957b0655c9c8e1ba8a67b13fd',1,'Car']]],
  ['coordy_240',['coordY',['../classCar.html#aaca3fa039d344bf4e14b46e99f1cfb0c',1,'Car']]],
  ['customscale_241',['customScale',['../classRoadsNetwork.html#a0fa840b28ab5568ee21129535362c4ba',1,'RoadsNetwork']]]
];
