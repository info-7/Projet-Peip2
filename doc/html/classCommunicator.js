var classCommunicator =
[
    [ "requete", "structCommunicator_1_1requete.html", "structCommunicator_1_1requete" ],
    [ "Communicator", "classCommunicator.html#abd4c26c6a901e571bc9cfc4259e65de3", null ],
    [ "~Communicator", "classCommunicator.html#a4ced5362bf7438924f8d7f1b0c5ec391", null ],
    [ "actionNon", "classCommunicator.html#a5c76b1f34f6df12fcf0098c8dcced142", null ],
    [ "actionOui", "classCommunicator.html#a2cb0c4cff7317ebc5ac13d2904d44556", null ],
    [ "actionPrix", "classCommunicator.html#afe57b168a413d62ecfa9086e032e8743", null ],
    [ "askModif", "classCommunicator.html#ac904053bf77e9eed82cd53f115894dd8", null ],
    [ "askPrice", "classCommunicator.html#acc829da7346664221b8b5b178dab96c9", null ],
    [ "processQueue", "classCommunicator.html#a785a7cc8abce5d5813cfacd77e430522", null ],
    [ "Qvaleur", "classCommunicator.html#a24e4870add12aa295876d32637875067", null ],
    [ "send", "classCommunicator.html#a1cd467b7e612431cb04e9b6bd23e3377", null ],
    [ "sendNo", "classCommunicator.html#a749f48373d3e75451a2b98b49412ff78", null ],
    [ "sendPrice", "classCommunicator.html#aad40df385dfc8f12bf05161e32e71cf4", null ],
    [ "sendYes", "classCommunicator.html#a31823d570edd2407121ab51da86f832d", null ],
    [ "buff", "classCommunicator.html#ab383c1c95ce38b3351076612e81126dd", null ],
    [ "R", "classCommunicator.html#aeea1f50e8aa3ed017a5ce6805104806f", null ],
    [ "valeur", "classCommunicator.html#a3a3e351c35abb07fed485bf9899c88bb", null ]
];