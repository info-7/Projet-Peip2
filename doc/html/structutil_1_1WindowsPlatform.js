var structutil_1_1WindowsPlatform =
[
    [ "WindowsPlatform", "structutil_1_1WindowsPlatform.html#a6f3766048e1807d75413ebcf026d8392", null ],
    [ "~WindowsPlatform", "structutil_1_1WindowsPlatform.html#a35ba68c2186ae242edb4186ad3449dde", null ],
    [ "getIconDirectory", "structutil_1_1WindowsPlatform.html#a827654d7876e44db57b0ac374c7a0ac8", null ],
    [ "getIconFromIconDirectory", "structutil_1_1WindowsPlatform.html#a06d62a3caed6b4cba7123c6c6ea52852", null ],
    [ "getRefreshRate", "structutil_1_1WindowsPlatform.html#a4b3656c1ca3cf7eab52bd8205729c5f4", null ],
    [ "getScreenScalingFactor", "structutil_1_1WindowsPlatform.html#a9cdc52b10354c5f3949aa08fb9c1eea6", null ],
    [ "setIcon", "structutil_1_1WindowsPlatform.html#a04ac02ad7151860559fb480e6190f895", null ],
    [ "sfmlWindowStyleToWin32WindowStyle", "structutil_1_1WindowsPlatform.html#a3888302e2c0bd4980bf36bf3aae4d5ea", null ],
    [ "toggleFullscreen", "structutil_1_1WindowsPlatform.html#a12d82194c51ac484ac01f3cc6f3652fc", null ],
    [ "m_hIcons", "structutil_1_1WindowsPlatform.html#ab58ad1516333926d3c2a7b101f6d9e6c", null ],
    [ "m_refreshRate", "structutil_1_1WindowsPlatform.html#ad18188d88e464b193074c60f908f649c", null ],
    [ "m_screenScalingFactor", "structutil_1_1WindowsPlatform.html#a41e3ef7678cbc8e58a5a47377796d28b", null ]
];