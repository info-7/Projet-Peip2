var classroadsNetwork =
[
    [ "roadsNetwork", "classroadsNetwork.html#a659e5fe0990eacbd67c0c221ed2a3c61", null ],
    [ "buildRoads", "classroadsNetwork.html#a76b69beafd4353764c64b246fc86a898", null ],
    [ "customScale", "classroadsNetwork.html#aa16d0aa7c4101b53fc4cf8ce93b59847", null ],
    [ "dx", "classroadsNetwork.html#a24689b4484b5dd06a04363c6adde7a8c", null ],
    [ "dy", "classroadsNetwork.html#ac7a98bbc5cbd0f81d06f14cf340047c3", null ],
    [ "hRoadsPoints", "classroadsNetwork.html#aecd0cab87d0b3dd8660b3caf855d8714", null ],
    [ "isPosUsed", "classroadsNetwork.html#aee8582979b28819ec2ffed6c403a4e47", null ],
    [ "posXIndex", "classroadsNetwork.html#ae559a0ed139101686031568bfd0ad826", null ],
    [ "posYIndex", "classroadsNetwork.html#a194f1f5e33785b8661e45b58e69fb98e", null ],
    [ "roadSize", "classroadsNetwork.html#af8c74ea2b442f3b3535668670c7e363b", null ],
    [ "vRoadsPoints", "classroadsNetwork.html#af46e7fa85f1411cb13532414ec19f728", null ],
    [ "window", "classroadsNetwork.html#aad434320f267ab2370a4cfb84a262f8e", null ]
];