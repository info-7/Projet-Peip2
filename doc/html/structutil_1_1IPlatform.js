var structutil_1_1IPlatform =
[
    [ "~IPlatform", "structutil_1_1IPlatform.html#a0871cdb2e3ba111b58d910af8d78f740", null ],
    [ "getRefreshRate", "structutil_1_1IPlatform.html#aaca0f786f9a1aad4cd016dd98fcd85b7", null ],
    [ "getScreenScalingFactor", "structutil_1_1IPlatform.html#aff4e642e55143a4d7925411ff56970ab", null ],
    [ "setIcon", "structutil_1_1IPlatform.html#aabac4c4af05614dee6065f226519bd94", null ],
    [ "toggleFullscreen", "structutil_1_1IPlatform.html#abc6f5e66cfbadd1b3111cde2c728d16c", null ]
];