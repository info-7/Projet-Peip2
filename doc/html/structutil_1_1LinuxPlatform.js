var structutil_1_1LinuxPlatform =
[
    [ "LinuxPlatform", "structutil_1_1LinuxPlatform.html#afc6f87ae98bc83da9f1502430286c98b", null ],
    [ "getRefreshRate", "structutil_1_1LinuxPlatform.html#a273f32b02d9158ffd57fa291ab7e2614", null ],
    [ "getScreenScalingFactor", "structutil_1_1LinuxPlatform.html#a6d74b5883c30117619b5cdc5589297d6", null ],
    [ "setIcon", "structutil_1_1LinuxPlatform.html#a5f091a688ddeefd2849104d04ae15f19", null ],
    [ "toggleFullscreen", "structutil_1_1LinuxPlatform.html#a5a546e4eb9f29381b1fb431fbacf8706", null ]
];